import 'dart:io';

import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SqliteHelper {
  static Database? _database;
  static final SqliteHelper db = SqliteHelper._();
  SqliteHelper._();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'movie_db.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db
          .execute(
              'CREATE TABLE users(id INTEGER PRIMARY KEY,email TEXT,password TEXT,username TEXT)')
          .then((value) {})
          .catchError((onError) {
        print('Error creating table $onError');
      });
    });
  }

  // insert data users
  createUser(User user) async {
    final db = await database;
    final res = await db.insert('users', user.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return res;
  }

  validateUser(String email, String password) async {
    final db = await database;
    final res = await db.rawQuery(
        "SELECT * from users where email = '$email' and password = '$password' LIMIT 1");
    List<User> list =
        res.isNotEmpty ? res.map((c) => User.fromJson(c)).toList() : [];

    return list;
  }

  validateUserByEmail(String email) async {
    final db = await database;
    final res =
        await db.rawQuery("SELECT * from users where email = '$email' LIMIT 1");
    List<User> list =
        res.isNotEmpty ? res.map((c) => User.fromJson(c)).toList() : [];

    return list;
  }
}

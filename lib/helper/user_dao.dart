import 'package:majootestcase/helper/sqlite_helper.dart';
import 'package:majootestcase/models/user.dart';

class UserDao {
  getUserByEmailAndPassword(String email, String password) async {
    return await SqliteHelper.db.validateUser(email, password);
  }

  getUserByEmail(String email) async {
    return await SqliteHelper.db.validateUserByEmail(email);
  }

  insertUser(User user) async {
    return await SqliteHelper.db.createUser(user);
  }
}

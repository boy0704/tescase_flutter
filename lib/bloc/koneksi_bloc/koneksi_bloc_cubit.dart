import 'dart:async';

import 'package:bloc/bloc.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'koneksi_bloc_state.dart';

class KoneksiBlocCubit extends Cubit<KoneksiBlocState> {
  KoneksiBlocCubit() : super(KoneksiBlocInitial());

  late StreamSubscription<DataConnectionStatus> listener;
  var internetStatus = "Unknown";
  var contentmessage = "Unknown";

  cekKoneksi(BuildContext context) async {
    listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.disconnected:
          emit(KoneksiBlocDisconnect());
          internetStatus = "Internet Kamu Sedang Offline. ";
          contentmessage = "Silahkan Cek Koneksi kamu sekarang";
          _showDialog(internetStatus, contentmessage, context);
          break;
        case DataConnectionStatus.connected:
          emit(KoneksiBlocConnect());
          break;
        default:
          print("checking connection");
      }
    });
    return await DataConnectionChecker().connectionStatus;
  }

  void _showDialog(String title, String content, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: new Text(title),
              content: new Text(content),
              actions: <Widget>[
                new ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: new Text("Tutup"))
              ]);
        });
  }

  void dispose() {
    listener.cancel();
  }
}

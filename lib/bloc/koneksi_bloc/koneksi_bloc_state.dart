part of 'koneksi_bloc_cubit.dart';

abstract class KoneksiBlocState extends Equatable {
  const KoneksiBlocState();

  @override
  List<Object> get props => [];
}

class KoneksiBlocInitial extends KoneksiBlocState {}

class KoneksiBlocConnect extends KoneksiBlocState {}

class KoneksiBlocDisconnect extends KoneksiBlocState {}

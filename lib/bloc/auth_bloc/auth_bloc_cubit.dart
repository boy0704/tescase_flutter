import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/helper/user_dao.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool(Preference.IS_LOGIN);
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState(false));
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState(true));
      } else {
        emit(AuthBlocLoginState(false));
      }
    }
  }

  void loginUser(User user) async {
    emit(AuthBlocLoadingState(true));
    var userDao = UserDao();
    var response =
        await userDao.getUserByEmailAndPassword(user.email!, user.password!);
    var totalData = response.length;
    if (totalData == 0) {
      emit(AuthBlocErrorState("Login gagal, periksa kembali inputan anda"));
      Future.delayed(Duration(seconds: 1), () {
        emit(AuthBlocLoadingState(false));
      });
    } else {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setBool(Preference.IS_LOGIN, true);
      sharedPreferences.setString(Preference.USERNAME, response[0].userName);
      emit(AuthBlocLoginState(true));
      emit(AuthBlocLoadingState(false));
      emit(AuthBlocSuccesState("Login Berhasil"));
    }
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoadingState(true));
    });
    await sharedPreferences.remove(Preference.IS_LOGIN);
    await sharedPreferences.remove(Preference.USERNAME);
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoadingState(false));
    });
  }

  void setInit() {
    emit(AuthBlocInitialState());
  }
}

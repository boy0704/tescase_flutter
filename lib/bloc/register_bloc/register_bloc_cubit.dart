import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/helper/user_dao.dart';
import 'package:majootestcase/models/user.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitial());

  void prosesRegister(User user) async {
    emit(RegisterLoading(true));
    var userDao = UserDao();
    var validationUser = await userDao.getUserByEmail(user.email!);
    if (validationUser.length > 0) {
      emit(RegisterLoading(false));
      emit(RegisterError(
          "Email ini sudah digunakan, silahkan gunakan email lain"));
    } else {
      var response = await userDao.insertUser(user);
      if (response > 0) {
        emit(RegisterLoading(false));
        emit(RegisterSuccess("Register Berhasil"));
      } else {
        emit(RegisterLoading(false));
        emit(RegisterError("Terjadi Kesalahan, silahkan coba lagi"));
      }
    }
  }

  void setInit() {
    emit(RegisterBlocInitial());
  }
}

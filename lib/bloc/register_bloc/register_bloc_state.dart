part of 'register_bloc_cubit.dart';

abstract class RegisterBlocState extends Equatable {
  const RegisterBlocState();

  @override
  List<Object> get props => [];
}

class RegisterBlocInitial extends RegisterBlocState {}

class RegisterLoading extends RegisterBlocState {
  final bool isLoading;

  const RegisterLoading(this.isLoading);

  @override
  List<Object> get props => [isLoading];
}

class RegisterSuccess extends RegisterBlocState {
  final String message;

  const RegisterSuccess(this.message);

  @override
  List<Object> get props => [message];
}

class RegisterError extends RegisterBlocState {
  final String message;

  const RegisterError(this.message);

  @override
  List<Object> get props => [message];
}

class RegisterAlreadyExists extends RegisterBlocState {
  final String message;

  const RegisterAlreadyExists(this.message);

  @override
  List<Object> get props => [message];
}

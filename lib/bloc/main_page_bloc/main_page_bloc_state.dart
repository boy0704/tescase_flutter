part of 'main_page_bloc_cubit.dart';

abstract class MainPageBlocState extends Equatable {
  const MainPageBlocState();

  @override
  List<Object> get props => [];
}

class OnMainPageBlocInitial extends MainPageBlocState {}

class OnSplashScreenPage extends MainPageBlocState {}

class OnLoginPage extends MainPageBlocState {}

class OnRegisterPage extends MainPageBlocState {}

class OnHomePage extends MainPageBlocState {
  final String username;
  OnHomePage(this.username);
  @override
  List<Object> get props => [username];
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'main_page_bloc_state.dart';

class MainPageBlocCubit extends Cubit<MainPageBlocState> {
  MainPageBlocCubit() : super(OnMainPageBlocInitial());

  void splashScreenPage() async {
    emit(OnSplashScreenPage());
  }

  void loginPage() async {
    emit(OnLoginPage());
  }

  void registerPage() async {
    emit(OnRegisterPage());
  }

  void homePage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? username = sharedPreferences.getString(Preference.USERNAME);
    emit(OnHomePage(username!));
  }

  void statusLogin() async {
    emit(OnMainPageBlocInitial());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool(Preference.IS_LOGIN);
    String? username = sharedPreferences.getString(Preference.USERNAME);

    print("state : $isLoggedIn");
    if (isLoggedIn == null) {
      emit(OnSplashScreenPage());
    } else {
      emit(OnHomePage(username!));
    }
  }
}

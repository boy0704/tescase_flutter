part of '../mainpage/pages.dart';

class PesanToast {
  pesanSukses(BuildContext context, String pesan) {
    Flushbar(
      title: "Sukses",
      flushbarPosition: FlushbarPosition.TOP,
      message: pesan,
      icon: Icon(
        Icons.check,
        size: 28.0,
        color: Colors.white,
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.green,
      boxShadows: [
        BoxShadow(
          color: Colors.red[800]!,
          offset: Offset(0.0, 2.0),
          blurRadius: 3.0,
        )
      ],
    )..show(context);
  }

  pesanError(BuildContext context, String pesan) {
    Flushbar(
      title: "Kesalahan",
      flushbarPosition: FlushbarPosition.TOP,
      message: pesan,
      icon: Icon(
        Icons.warning,
        size: 28.0,
        color: Colors.white,
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.red,
      boxShadows: [
        BoxShadow(
          color: Colors.red[800]!,
          offset: Offset(0.0, 2.0),
          blurRadius: 3.0,
        )
      ],
    )..show(context);
  }

  pesanTunggu(BuildContext context, String pesan) {
    Flushbar(
      title: "Tunggu",
      flushbarPosition: FlushbarPosition.TOP,
      message: pesan,
      icon: Icon(
        Icons.info_outline,
        size: 28.0,
        color: Colors.white,
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Colors.green,
      boxShadows: [
        BoxShadow(
          color: Colors.red[800]!,
          offset: Offset(0.0, 2.0),
          blurRadius: 3.0,
        )
      ],
    )..show(context);
  }
}

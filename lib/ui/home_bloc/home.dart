part of '../mainpage/pages.dart';

class HomeMovie extends StatefulWidget {
  @override
  _HomeMovieState createState() => _HomeMovieState();
}

class _HomeMovieState extends State<HomeMovie> {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<KoneksiBlocCubit>(context)..cekKoneksi(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.account_circle_outlined, color: Colors.black),
                onPressed: () {})
          ],
          leading: IconButton(
              icon: Icon(Icons.menu, color: Colors.black), onPressed: () {}),
          title: Text(
            "Majoo Movie",
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            BlocBuilder<KoneksiBlocCubit, KoneksiBlocState>(
              builder: (context, state) {
                if (state is KoneksiBlocInitial) {
                  return LoadingIndicator();
                } else if (state is KoneksiBlocDisconnect) {
                  return Container(
                    child: Center(
                      child: Text("Koneksi Tidak Normal"),
                    ),
                  );
                } else {
                  return BlocBuilder<HomeBlocCubit, HomeBlocState>(
                      builder: (context, state) {
                    if (state is HomeBlocLoadedState) {
                      return HomeBlocLoadedScreen(data: state.data);
                    } else if (state is HomeBlocLoadingState) {
                      return LoadingIndicator();
                    } else if (state is HomeBlocInitialState) {
                      return Text("Tunggu..");
                    } else if (state is HomeBlocErrorState) {
                      return ErrorScreen(message: state.error);
                    }

                    return Center(
                        child: Text(
                            kDebugMode ? "state not implemented $state" : ""));
                  });
                }
              },
            ),
          ]),
        ),
      ),
    );
  }
}

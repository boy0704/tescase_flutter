part of '../mainpage/pages.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<KoneksiBlocCubit>(context)..cekKoneksi(context);
    return BlocBuilder<KoneksiBlocCubit, KoneksiBlocState>(
      builder: (context, state) {
        if (state is KoneksiBlocDisconnect) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              actions: <Widget>[
                PopupMenuButton(
                  onSelected: (value) {
                    BlocProvider.of<AuthBlocCubit>(context)..logout();
                    BlocProvider.of<MainPageBlocCubit>(context)
                      ..splashScreenPage();
                  },
                  offset: const Offset(0, 300),
                  icon: Icon(Icons.account_circle_outlined,
                      color: Color(0xFF2c406e)),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: 0,
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.logout, color: ColorPalette.primaryColor),
                          SizedBox(width: 8.0),
                          Text("Logout",
                              style:
                                  TextStyle(color: ColorPalette.primaryColor))
                        ],
                      ),
                    ),
                  ],
                ),
              ],
              leading: IconButton(
                  icon: Icon(Icons.menu, color: Colors.black),
                  onPressed: () {}),
              title: Text(
                "Majoo Movie",
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Lottie.asset('assets/lottie/no_koneksi.json',
                      width: 200, height: 200, fit: BoxFit.fill),
                  SizedBox(
                    height: 16,
                  ),
                  Text("Tidak Ada Internet",
                      style: TextStyle(color: ColorPalette.primaryColor)),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: ColorPalette.primaryColor),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.refresh_rounded,
                              color: Colors.white,
                            ),
                            Text("Refresh",
                                style: TextStyle(
                                  color: Colors.white,
                                ))
                          ],
                        ),
                        onPressed: () {
                          BlocProvider.of<HomeBlocCubit>(context)
                            ..fetchingData();
                        }),
                  )
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              actions: <Widget>[
                PopupMenuButton(
                  onSelected: (value) {
                    BlocProvider.of<AuthBlocCubit>(context)..logout();
                    BlocProvider.of<MainPageBlocCubit>(context)
                      ..splashScreenPage();
                  },
                  offset: const Offset(0, 300),
                  icon: Icon(Icons.account_circle_outlined,
                      color: Color(0xFF2c406e)),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: 0,
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.logout, color: ColorPalette.primaryColor),
                          SizedBox(width: 8.0),
                          Text("Logout",
                              style:
                                  TextStyle(color: ColorPalette.primaryColor))
                        ],
                      ),
                    ),
                  ],
                ),
              ],
              leading: IconButton(
                  icon: Icon(Icons.menu, color: Colors.black),
                  onPressed: () {}),
              title: Text(
                "Majoo Movie",
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 24, top: 12),
                    child: Text(
                      "List Movies",
                      style: TextStyle(color: ColorPalette.primaryColor),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(12, 24, 12, 15),
                    height: MediaQuery.of(context).size.height -
                        MediaQuery.of(context).padding.top -
                        MediaQuery.of(context).padding.bottom -
                        100,
                    child: GridView.builder(
                      semanticChildCount: 2,
                      physics: ScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 12,
                        mainAxisSpacing: 12,
                        childAspectRatio: 0.7,
                      ),
                      itemCount: data?.length ?? 0,
                      itemBuilder: (context, index) {
                        if (data!.length <= 0) {
                          return CircularProgressIndicator();
                        }
                        return GestureDetector(
                            onTap: () {
                              BlocProvider.of<HomeBlocCubit>(context)
                                ..detailMovie(data![index]);
                            },
                            child: MovieCard(
                              data: data![index],
                            ));
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        }
      },
    );
  }
}

part of '../mainpage/pages.dart';

// ignore: must_be_immutable
class DetailMovie extends StatefulWidget {
  Data? data;
  DetailMovie(this.data);

  @override
  _DetailMovieState createState() => _DetailMovieState();
}

class _DetailMovieState extends State<DetailMovie> {
  List<dynamic>? imageSeries;
  @override
  void setState(fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
        return Future.value(true);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: IconButton(
                icon: Icon(Icons.chevron_left, color: Colors.black),
                onPressed: () {
                  BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
                  return;
                }),
            title: Text(
              "Majoo Movie",
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.all(12),
                  child: movieItemWidget(widget.data!),
                ),
                SizedBox(height: 100),
              ],
            ),
          )),
    );
  }

  Widget movieItemWidget(Data? data) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(25),
          child: Image.network(data?.i?.imageUrl ?? ""),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
          child: Text("Title : ${data?.l}", textDirection: TextDirection.ltr),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
          child: Text("Year : ${data?.year}", textDirection: TextDirection.ltr),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
          child: Text("List Series Images And Title : ",
              textDirection: TextDirection.ltr),
        ),
        data?.series != null
            ? ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: data?.series == null ? 0 : data!.series!.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Image.network(data?.series?[index].i?.imageUrl ?? ""),
                      Text("${data?.series?[index].l}\n")
                    ],
                  );
                })
            : Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text("Not Found", textDirection: TextDirection.ltr),
              ),
      ],
    );
  }
}

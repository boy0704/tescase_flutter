part of '../mainpage/pages.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<KoneksiBlocCubit>(context)..cekKoneksi(context);
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocInitialState) {
        return Center(
          child: SpinKitFadingCircle(
            color: ColorPalette.primaryColor,
          ),
        );
      }
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocLoadingState) {
        return Center(
          child: SpinKitFadingCircle(
            color: ColorPalette.primaryColor,
          ),
        );
      } else if (state is HomeBlocInitialState) {
        return Scaffold();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(message: state.error);
      } else if (state is BlocDetailMovie) {
        return DetailMovie(state.data);
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}

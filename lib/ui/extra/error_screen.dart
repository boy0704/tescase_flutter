import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/main_bloc.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String? message;
  final Function()? retry;
  final Color? textColor;
  final double? fontSize;
  final double? gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<HomeBlocCubit>(
        create: (_) => HomeBlocCubit(),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                message!,
                style:
                    TextStyle(fontSize: 12, color: textColor ?? Colors.black),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: ColorPalette.primaryColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.refresh_rounded,
                          color: Colors.white,
                        ),
                        Text("Refresh",
                            style: TextStyle(
                              color: Colors.white,
                            ))
                      ],
                    ),
                    onPressed: () {
                      BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}

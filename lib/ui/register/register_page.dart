part of '../mainpage/pages.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool isEmailValid = true;
  bool isUsernameValid = true;
  bool isPasswordValid = true;
  bool isRegisterIn = false;
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BlocProvider.of<MainPageBlocCubit>(context)..loginPage();
        return Future.value(true);
      },
      child: Scaffold(
        body: BlocProvider(
          create: (_) => RegisterBlocCubit(),
          child: BlocBuilder<RegisterBlocCubit, RegisterBlocState>(
            builder: (context, state) {
              if (state is RegisterError) {
                Future.microtask(() async =>
                    await PesanToast().pesanError(context, state.message));
              } else if (state is RegisterSuccess) {
                Future.microtask(() async {
                  await PesanToast().pesanSukses(context, state.message);
                  BlocProvider.of<MainPageBlocCubit>(context)..loginPage();
                });
              } else if (state is RegisterAlreadyExists) {
                Future.microtask(() async =>
                    await PesanToast().pesanError(context, state.message));
              }
              return Container(
                color: ColorPalette.primaryColor,
                padding: EdgeInsets.all(20.0),
                child: ListView(
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: <Widget>[
                          _iconRegister(),
                          _titleDescription(),
                          _textField(context),
                          Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 32.0),
                              ),
                              (state is RegisterLoading) == true
                                  ? SpinKitFadingCircle(color: Colors.white)
                                  : InkWell(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 15.0),
                                        width: double.infinity,
                                        child: Text(
                                          'Register Sekarang',
                                          style: TextStyle(
                                              color: ColorPalette.primaryColor),
                                          textAlign: TextAlign.center,
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                      ),
                                      onTap: () {
                                        if (_emailController.text.isEmpty ||
                                            _passwordController.text.isEmpty ||
                                            _usernameController.text.isEmpty) {
                                          PesanToast().pesanError(context,
                                              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                                        } else if (isEmailValid == false) {
                                          PesanToast().pesanError(context,
                                              "Masukkan e-mail yang valid");
                                        } else if (isPasswordValid == false) {
                                          PesanToast().pesanError(context,
                                              "Password Kurang dari 6 Karakter");
                                        } else {
                                          print("state : Proses Register");
                                          User user = User(
                                              email: _emailController.text,
                                              password:
                                                  _passwordController.text,
                                              userName:
                                                  _usernameController.text);
                                          BlocProvider.of<RegisterBlocCubit>(
                                              context)
                                            ..prosesRegister(user);
                                        }
                                      },
                                    ),
                              Padding(
                                padding: EdgeInsets.only(top: 16.0),
                              ),
                              Text(
                                'or',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12.0,
                                ),
                              ),
                              ElevatedButton(
                                  child: Text(
                                    'Login',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: () {
                                    BlocProvider.of<MainPageBlocCubit>(context)
                                      ..loginPage();
                                  }),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _iconRegister() {
    return Image.asset(
      "assets/images/icon_register.png",
      width: 150.0,
      height: 150.0,
    );
  }

  Widget _titleDescription() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        Text(
          "Registration",
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
      ],
    );
  }

  Widget _textField(context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
        TextFormField(
          onChanged: (text) {
            setState(() {
              isUsernameValid = text.length > 0;
            });
            context.bloc<AuthBlocCubit>()..setInit();
          },
          controller: _usernameController,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3.0,
              ),
            ),
            hintText: "Username",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          autofocus: false,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Visibility(
              child: Text(
                "Username Tidak Boleh Kosong",
                style: TextStyle(color: Colors.red),
              ),
              visible: !isUsernameValid),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
        TextFormField(
          onChanged: (text) {
            setState(() {
              isEmailValid = EmailValidator.validate(text);
            });
            context.bloc<RegisterBlocCubit>()..setInit();
          },
          controller: _emailController,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3.0,
              ),
            ),
            hintText: "Email",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          autofocus: false,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Visibility(
              child: Text(
                "Email Tidak Valid",
                style: TextStyle(color: Colors.red),
              ),
              visible: !isEmailValid),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
        TextFormField(
          onChanged: (text) {
            setState(() {
              isPasswordValid = text.length >= 6;
            });
            context.bloc<RegisterBlocCubit>()..setInit();
          },
          controller: _passwordController,
          obscureText: _isObscurePassword,
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            suffixIcon: IconButton(
                color: Colors.white,
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                )),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
                width: 3.0,
              ),
            ),
            hintText: "Password",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: Colors.white),
          autofocus: false,
        ),
        Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Visibility(
                child: Text(
                  "Password Minimal 6 Karakter",
                  style: TextStyle(color: Colors.red),
                ),
                visible: !isPasswordValid)),
      ],
    );
  }
}

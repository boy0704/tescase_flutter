part of '../mainpage/pages.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool isEmailValid = true;
  bool isPasswordValid = true;
  bool isSigningIn = false;
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<AuthBlocCubit>(
      create: (_) => AuthBlocCubit(),
      child:
          BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
        if (state is AuthBlocErrorState) {
          Future.microtask(
              () async => await PesanToast().pesanError(context, state.error));
        } else if (state is AuthBlocSuccesState) {
          Future.microtask(() async =>
              await PesanToast().pesanSukses(context, state.message));
          BlocProvider.of<MainPageBlocCubit>(context)..homePage();
        } else if (state is AuthBlocLoadingState) {
          if (state.isLoading == true) {
            isSigningIn = true;
          } else {
            isSigningIn = false;
          }
        } else if (state is AuthBlocInitialState) {
          isSigningIn = false;
        }
        return Container(
          color: Colors.white,
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  children: <Widget>[
                    _iconLogin(),
                    _titleDescription(),
                    _textField(context),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 24.0),
                        ),
                        isSigningIn
                            ? SpinKitFadingCircle(
                                color: ColorPalette.primaryColor,
                              )
                            : InkWell(
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 15.0),
                                  width: double.infinity,
                                  child: Text(
                                    'Login',
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                  decoration: BoxDecoration(
                                    color: ColorPalette.primaryColor,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                ),
                                onTap: () async {
                                  if (_emailController.text == "" ||
                                      _passwordController.text == "") {
                                    PesanToast().pesanError(context,
                                        "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
                                    setState(() {
                                      isEmailValid = false;
                                      isPasswordValid = false;
                                    });
                                  } else if (isEmailValid == false) {
                                    PesanToast().pesanError(
                                        context, "Masukkan e-mail yang valid");
                                  } else {
                                    User user = User(
                                        email: _emailController.text,
                                        password: _passwordController.text);
                                    BlocProvider.of<AuthBlocCubit>(context)
                                      ..loginUser(user);
                                    if (state is AuthBlocLoadingState) {
                                      setState(() {
                                        isSigningIn = state.isLoading;
                                      });
                                    }
                                  }
                                },
                              ),
                        Padding(
                          padding: EdgeInsets.only(top: 100.0),
                        ),
                        Container(height: 0.5, color: Colors.grey),
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        Text(
                          'or',
                          style: TextStyle(
                            color: ColorPalette.primaryColor,
                            fontSize: 12.0,
                          ),
                        ),
                        ElevatedButton(
                          child: Text(
                            'Register',
                            style: TextStyle(
                                fontSize: 18, color: ColorPalette.primaryColor),
                          ),
                          onPressed: () {
                            // PesanToast().pesanSukses(context, "Berhasil login");
                            BlocProvider.of<MainPageBlocCubit>(context)
                              ..registerPage();
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    ));
  }

  Widget _iconLogin() {
    return Image.asset(
      "assets/images/icon_login.png",
      width: 150.0,
      height: 150.0,
    );
  }

  Widget _titleDescription() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        Text(
          "Silahkan Login disini",
          style: TextStyle(
            color: ColorPalette.primaryColor,
            fontSize: 16.0,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
      ],
    );
  }

  Widget _textField(context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
        TextFormField(
          onChanged: (text) {
            setState(() {
              isEmailValid = EmailValidator.validate(text);
            });
            context.bloc<AuthBlocCubit>()..setInit();
          },
          controller: _emailController,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.primaryColor,
                width: 1.0,
              ),
            ),
            labelText: "Email",
            labelStyle: TextStyle(color: ColorPalette.hintColor),
            hintText: "Masukkan Email",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: ColorPalette.primaryColor),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Visibility(
              child: Text(
                "Masukkan Email yang valid",
                style: TextStyle(color: Colors.red),
              ),
              visible: !isEmailValid),
        ),
        SizedBox(
          height: 16,
        ),
        TextFormField(
          onChanged: (text) {
            setState(() {
              isPasswordValid = text.length >= 6;
            });
            context.bloc<AuthBlocCubit>()..setInit();
          },
          controller: _passwordController,
          obscureText: _isObscurePassword,
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underlineTextField,
                width: 1.5,
              ),
            ),
            suffixIcon: IconButton(
                color: ColorPalette.primaryColor,
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                )),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.primaryColor,
                width: 1.0,
              ),
            ),
            labelText: "Password",
            labelStyle: TextStyle(color: ColorPalette.hintColor),
            hintText: "Password",
            hintStyle: TextStyle(color: ColorPalette.hintColor),
          ),
          style: TextStyle(color: ColorPalette.primaryColor),
          autofocus: false,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Visibility(
              child: Text(
                "Password Minimal 6 Karakter",
                style: TextStyle(color: Colors.red),
              ),
              visible: !isPasswordValid),
        ),
      ],
    );
  }
}

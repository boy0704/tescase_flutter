import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lottie/lottie.dart';
import 'package:majootestcase/bloc/main_bloc.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';

import 'package:majootestcase/models/movie_response.dart';

import 'package:flutter/foundation.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

part '../login/login_page.dart';
part '../register/register_page.dart';
part '../home_bloc/home.dart';
part '../home_bloc/home_bloc_loaded_screen.dart';
part '../home_bloc/home_bloc_screen.dart';
part '../home_bloc/detail_movie.dart';
part '../widget/pesan_toast.dart';
part '../widget/movie_card.dart';

part 'main_pages.dart';

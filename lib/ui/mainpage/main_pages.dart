part of 'pages.dart';

class MainPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MainPageBlocCubit>(context)..statusLogin();

    return Scaffold(body: BlocBuilder<MainPageBlocCubit, MainPageBlocState>(
      builder: (_, state) {
        print("state : $state");
        if (state is OnHomePage) {
          BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
          return HomeBlocScreen();
          // return HomeMovie();
        } else if (state is OnRegisterPage) {
          return RegisterPage();
        } else if (state is OnSplashScreenPage) {
          return SplashScreenPage();
        } else if (state is OnLoginPage) {
          return LoginPage();
        } else {
          return SplashScreenPage();
        }
      },
    ));
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.primaryColor,
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/splash.png",
            width: 150.0,
            height: 150.0,
          ),
          Text(
            "Majoo Movie",
            style: TextStyle(
                color: Colors.red, fontSize: 25, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: InkWell(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15.0),
                width: double.infinity,
                child: Text(
                  'Mulai',
                  style: TextStyle(color: ColorPalette.primaryColor),
                  textAlign: TextAlign.center,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
              onTap: () {
                BlocProvider.of<MainPageBlocCubit>(context)..loginPage();
              },
            ),
          )
        ],
      )),
    );
  }
}

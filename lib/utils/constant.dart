import 'dart:ui';

class Preference {
  static const IS_LOGIN = "is_logged_in";
  static const USERNAME = "username";

}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class ImageUrl {
  static const SPLASH_IMAGE =
      "https://www.kindpng.com/picc/m/30-300778_transparent-movie-marquee-png-movie-icon-png-flat.png";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class ColorPalette {
  static const primaryColor = Color(0xff5364e8);
  static const primaryDarkColor = Color(0xff607Cbf);
  static const underlineTextField = Color(0xff8b97ff);
  static const hintColor = Color(0xffccd1ff);
}

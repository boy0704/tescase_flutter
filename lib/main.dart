import 'package:majootestcase/bloc/main_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/ui/mainpage/pages.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Test Flutter',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => MainPageBlocCubit()),
            BlocProvider(create: (_) => AuthBlocCubit()),
            BlocProvider(create: (_) => RegisterBlocCubit()),
            BlocProvider(create: (_) => HomeBlocCubit()),
            BlocProvider(create: (_) => KoneksiBlocCubit())
          ],
          child: MainPages(),
        ));
  }
}
